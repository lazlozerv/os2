#include <stdlib.h>
#include <stdio.h>
#include "../inc/semph.h"

int sem_init() {	//initialiizng 3 semaphores	
        int semid;

        if ((semid = semget(IPC_PRIVATE,3,IPC_CREAT | 0600)) < 0) {
                perror("Creating semaphore failed");
                exit(1);
        }

        
        if (semctl(semid,EMPTY,SETVAL,0) < 0) {
                perror("Creating new semaphore failed");
                exit(1);
        }

        if (semctl(semid,EMPTY1,SETVAL,0) < 0) {
                perror("Creating new semaphore failed");
                exit(1);
        }
        
        if (semctl(semid,EMPTY2,SETVAL,0) < 0) {
                perror("Creating new semaphore failed");
                exit(1);
        }      

        return semid;
}

void wat(int semid,int index) {
        struct sembuf sops[1];

        sops[0].sem_num=index;
        sops[0].sem_op=-1;
        sops[0].sem_flg=0;

        if (semop(semid,sops,1) == -1) {
                perror("Wait failed");
                exit(1);
        }
}

void signl(int semid,int index) {
        struct sembuf sops[1];

        sops[0].sem_num=index;
        sops[0].sem_op=1;
        sops[0].sem_flg=0;

        if (semop(semid,sops,1) == -1) {
                perror("Wait failed");
                exit(1);
        }
}


