#include <iostream>
#include "../inc/func_strct.h"
#include "../inc/hash.h"


Hash::Hash(int b) 
{ 
    BUCKET = b; 
    table = new std::vector<Node>[BUCKET]; 
} 

int Hash::hashFunction(int x) { 
        return (x % BUCKET); 
} 
  
int Hash::Search(address ad) {
  ad.addr=ad.addr >> 12 ;
  int index = hashFunction(ad.addr);
  for(int i=0; i<table[index].size(); i++) {
    if(table[index][i].page==ad.addr) {
      return i;
    }
  }
  return -1;
}

int Hash::Flush() {
  int DiskWrites=0;
  for(int i=0; i<BUCKET; i++) {
    for(int j=0; j<table[i].size(); j++) {
        DiskWrites+=table[i][j].isWritten;
    }
    table[i].clear();
  }
  return DiskWrites;
}

void Hash::updateBit(int page,int ind) {
  page=page >> 12 ;
  int index = hashFunction(page);
  table[index][ind].isWritten=1;
}

void Hash::insertItem(Node n) 
{ 
    int index = hashFunction(n.page); 
    table[index].push_back(n);  
} 
  
void Hash::deleteItem(Node n) 
{ 
  // get the hash index of key 
  int index = hashFunction(n.page); 
  
  // find the key in (inex)th list 
  std::vector <Node> :: iterator i; 
  for (i = table[index].begin(); 
           i != table[index].end(); i++) { 
    if ((*i).page == n.page) 
      break; 
  } 
  
  // if key is found in hash table, remove it 
  if (i != table[index].end()) 
    table[index].erase(i); 
} 
  
// function to display hash table 
void Hash::displayHash() { 
  for (int i = 0; i < BUCKET; i++) { 
    std::cout << i; 
    for (Node x : table[i]) 
      std::cout << " --> " << x.page; 
    std::cout << std::endl; 
  } 
} 

Hash::~Hash() {
    delete []table;
}